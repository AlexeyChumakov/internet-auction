﻿using AutoMapper;
using Business_Logic_Layer.Infrastructure;
using Business_Logic_Layer.Interfaces;
using Business_Logic_Layer.Models;
using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using Microsoft.AspNet.Identity;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Business_Logic_Layer.Services
{
	sealed class UserService : IUserService
	{
		private readonly IUnitOfWork unitOfWork;
		private readonly IMapper mapper;

		public UserService(IUnitOfWork unitOfWork, IMapper mapper)
		{
			this.unitOfWork = unitOfWork;
			this.mapper = mapper;
		}

		public async Task<ClaimsIdentity> Authenticate(UserModel userModel)
		{
			ClaimsIdentity claim = null;
			ApplicationUser user = null;

			// находим пользователя
			if (userModel.UserName is null && !(userModel.Email is null))
			{
				ApplicationUser userByEmail = await unitOfWork.UserManager.FindByEmailAsync(userModel.Email);
				user = await unitOfWork.UserManager.FindAsync(userByEmail.UserName, userModel.Password);
			}
			else if(userModel.Email is null && !(userModel.UserName is null))
			{
				user = await unitOfWork.UserManager.FindAsync(userModel.UserName, userModel.Password);
			}

			// авторизуем его и возвращаем объект ClaimsIdentity
			if (!(user is null))
				claim = await unitOfWork.UserManager.CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);

			return claim;
		}

		public async Task<IList<RegistrationError>> Create(UserModel userModel)
		{
			List<RegistrationError> registrationResults = new List<RegistrationError>();

			ApplicationUser userByUsername = await unitOfWork.UserManager.FindByNameAsync(userModel.UserName);
			ApplicationUser userByEmail = await unitOfWork.UserManager.FindByEmailAsync(userModel.Email);

			if (userByUsername is null && userByEmail is null)
			{
				ApplicationUser user = mapper.Map<UserModel, ApplicationUser>(userModel);

				IdentityResult result = await unitOfWork.UserManager.CreateAsync(user, userModel.Password);

				if (result.Errors.Any())
				{
					registrationResults.Add(new RegistrationError(result.Errors.FirstOrDefault(), ""));
				}
				else
				{
					await unitOfWork.UserManager.AddToRoleAsync(user.Id, userModel.Role);
					await unitOfWork.SaveAsync();

					return registrationResults;
				}
			}
			else
			{
				if(!(userByEmail is null))
					registrationResults.Add(new RegistrationError("This email address is already being used", "Email"));

				if(!(userByUsername is null))
					registrationResults.Add(new RegistrationError("User with this username already exists", "Username"));
			}

			return registrationResults;
		}

		public async Task<IList<CountryModel>> GetAllCountriesAsync()
		{
			return (await unitOfWork.CountryRepository.GetAllAsync()).Select(x => mapper.Map<Country, CountryModel>(x)).ToList();
		}

		public async Task<IList<GenderModel>> GetAllGendersAsync()
		{
			return (await unitOfWork.GenderRepository.GetAllAsync()).Select(x => mapper.Map<Gender, GenderModel>(x)).ToList();
		}

		public void Dispose()
		{
			unitOfWork?.Dispose();
		}
	}
}
