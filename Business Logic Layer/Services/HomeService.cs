﻿using AutoMapper;
using Business_Logic_Layer.Interfaces;
using Business_Logic_Layer.Models;
using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Business_Logic_Layer.Services
{
	public class HomeService : IHomeService
	{
		private readonly IUnitOfWork unitOfWork;
		private readonly IMapper mapper;

		public HomeService(IUnitOfWork unitOfWork, IMapper mapper)
		{
			this.unitOfWork = unitOfWork;
			this.mapper = mapper;
		}

		public async Task<IList<CategoryModel>> GetAllCategoriesAsync()
		{
			IList<Category> categories = await unitOfWork.CategoryRepository.GetAllAsync();

			return categories.Select(x => mapper.Map<Category, CategoryModel>(x)).ToList();
		}
	}
}
