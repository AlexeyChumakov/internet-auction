﻿namespace Business_Logic_Layer.Infrastructure
{
	public class RegistrationError
	{
        public string Message { get; }
        public string Property { get; }

        public RegistrationError(string message, string property)
        {
            Message = message;
            Property = property;
        }
    }
}
