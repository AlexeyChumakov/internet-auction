﻿using Business_Logic_Layer.Infrastructure;
using Business_Logic_Layer.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Business_Logic_Layer.Interfaces
{
    public interface IUserService : IDisposable
    {
        Task<IList<RegistrationError>> Create(UserModel userModel);
        Task<ClaimsIdentity> Authenticate(UserModel userModel);

        Task<IList<CountryModel>> GetAllCountriesAsync();
        Task<IList<GenderModel>> GetAllGendersAsync();
    }
}
