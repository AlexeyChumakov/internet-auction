﻿using Business_Logic_Layer.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Business_Logic_Layer.Interfaces
{
	public interface IHomeService
	{
		Task<IList<CategoryModel>> GetAllCategoriesAsync();
	}
}
