﻿using Business_Logic_Layer.Interfaces;
using Business_Logic_Layer.Services;
using Ninject.Modules;

namespace Business_Logic_Layer.IoC_Modules
{
	public class UserModule : NinjectModule
	{
		public override void Load()
		{
			Bind<IUserService>().To<UserService>();
		}
	}
}
