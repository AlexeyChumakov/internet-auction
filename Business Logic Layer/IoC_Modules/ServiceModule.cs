﻿using Data_Access_Layer;
using Data_Access_Layer.Interfaces;
using Ninject.Modules;

namespace Business_Logic_Layer.IoC_Modules
{
	public class ServiceModule : NinjectModule
    {
        private readonly string connectionString;

        public ServiceModule(string connectionString)
        {
            this.connectionString = connectionString;
        }
        public override void Load()
        {
            Bind<IUnitOfWork>().To<UnitOfWork>().WithConstructorArgument("connectionString", connectionString);
        }
    }
}
