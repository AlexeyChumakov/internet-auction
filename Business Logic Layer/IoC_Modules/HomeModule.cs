﻿using Business_Logic_Layer.Interfaces;
using Business_Logic_Layer.Services;
using Ninject.Modules;

namespace Business_Logic_Layer.IoC_Modules
{
	public class HomeModule : NinjectModule
	{
		public override void Load()
		{
			Bind<IHomeService>().To<HomeService>();
		}
	}
}
