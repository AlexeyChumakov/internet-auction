﻿using AutoMapper;
using Ninject.Modules;

namespace Business_Logic_Layer.IoC_Modules
{
	public class AutomapperModule : NinjectModule
    {
        public override void Load()
        {
            Bind<IMapper>().ToMethod(context =>
            {
                var myProfile = new AutomapperProfile();
                var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

                return new Mapper(configuration);
            });
        }
    }
}
