﻿namespace Business_Logic_Layer.Models
{
	public class CountryModel
	{
		public int Id { get; set; }
		public string Name { get; set; }
	}
}
