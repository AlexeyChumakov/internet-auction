﻿using System;

namespace Business_Logic_Layer.Models
{
	public class UserModel
	{
        public string Email { get; set; }
        public string Password { get; set; }
        public string UserName { get; set; }
        public string Role { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string PhoneNumber { get; set; }
        public string AboutMe { get; set; }
        public DateTime Birthdate { get; set; }

        public int GenderId { get; set; }
        public int CountryId { get; set; }
    }
}
