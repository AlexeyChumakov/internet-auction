﻿using System.Collections.Generic;

namespace Business_Logic_Layer.Models
{
	public class CategoryModel
	{
		public string Name { get; set; }
		public List<string> Subcategories { get; set; }
	}
}
