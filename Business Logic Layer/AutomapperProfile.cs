﻿using AutoMapper;
using Business_Logic_Layer.Models;
using Data_Access_Layer.Entities;
using System.Linq;

namespace Business_Logic_Layer
{
	class AutomapperProfile : Profile
	{
		public AutomapperProfile()
		{
			CreateMap<ApplicationUser, UserModel>().ReverseMap();
			CreateMap<Gender, GenderModel>().ReverseMap();
			CreateMap<Country, CountryModel>().ReverseMap();

			CreateMap<Category, CategoryModel>().
				ForMember(p => p.Subcategories, c => c.MapFrom(article => article.Subcategories.Select(x => x.Name)))
				.ReverseMap();
		}
	}
}
