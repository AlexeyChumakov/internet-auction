﻿using System.Data.Entity;
using Data_Access_Layer.Entities;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data_Access_Layer
{
	public class AuctionDbContext : IdentityDbContext<ApplicationUser>
    {
        public AuctionDbContext(string conectionString) : base(conectionString)
        {
            Database.SetInitializer(new AuctionDbInitializer());
        }

        public DbSet<Country> Countries  { get; set; }
        public DbSet<Gender> Genders { get; set; }

        public DbSet<Lot> Lots { get; set; }
        public DbSet<Auction> Auctions { get; set; }
        public DbSet<Bid> Bids { get; set; }
        public DbSet<Duration> Durations { get; set; }
        public DbSet<Category> Categories { get; set; }
        public DbSet<Subcategory> Subcategories { get; set; }
    }
}
