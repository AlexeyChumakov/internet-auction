﻿using Data_Access_Layer.Entities;
using Data_Access_Layer.Repositories;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;

namespace Data_Access_Layer
{
	class AuctionDbInitializer : DropCreateDatabaseIfModelChanges<AuctionDbContext>
	{
		protected override void Seed(AuctionDbContext context)
		{
			List<Category> defaultCategories = GetDefaultCategories();

			defaultCategories.ForEach(s => context.Categories.Add(s));
			context.SaveChanges();

			List<Subcategory> defaultSubcategories = GetDefaultSubcategories();

			defaultSubcategories.ForEach(s => context.Subcategories.Add(s));
			context.SaveChanges();



			List<Gender> defaultGenders = new List<Gender>()
			{
				new Gender { Name = "Male"},
				new Gender { Name = "Female"}
			};

			defaultGenders.ForEach(s => context.Genders.Add(s));
			context.SaveChanges();

			List<Country> defaultCountries = GetDefaultCountries();

			defaultCountries.ForEach(s => context.Countries.Add(s));
			context.SaveChanges();



			ApplicationUserManager userManager = new ApplicationUserManager(context);
			ApplicationRoleManager roleManager = new ApplicationRoleManager(context);

			ApplicationRole role1 = new ApplicationRole { Name = "User" };
			ApplicationRole role2 = new ApplicationRole { Name = "Admin" };

			roleManager.Create(role1);
			roleManager.Create(role2);

			ApplicationUser user1 = new ApplicationUser { 
				Email = "AlexeyChumakov2001@gmail.com",
				UserName = "Alex",
				Name = "Alexey",
				Surname = "Chumakov",
				PhoneNumber = "+380681106843",
				Birthdate = new DateTime(2001, 6, 2),
				GenderId = 1,
				CountryId = 230
			};
			string password1 = "Ea13_579";
			var result1 = userManager.Create(user1, password1);

			if (result1.Succeeded)
			{
				userManager.AddToRole(user1.Id, role1.Name);
			}

			ApplicationUser user2 = new ApplicationUser 
			{ 
				Email = "Petrov299145@gmail.com", 
				UserName = "Petr1998",
				Name = "Petr",
				Surname = "Petrenko",
				PhoneNumber = "+48536745633",
				Birthdate = new DateTime(1998, 11, 24),
				GenderId = 1,
				CountryId = 176
			};
			string password2 = "PwnM+m4Rp";
			var result2 = userManager.Create(user2, password2);

			if (result2.Succeeded)
			{
				userManager.AddToRole(user2.Id, role1.Name);
			}

			ApplicationUser user3 = new ApplicationUser
			{
				Email = "Ivan99Ivanov@gmail.com",
				UserName = "IvanIvanov",
				Name = "Ivan",
				Surname = "Ivanov",
				PhoneNumber = "+12025550143",
				Birthdate = new DateTime(1974,3,7),
				GenderId = 1,
				CountryId = 233
			};
			string password3 = "HTaL$zgCs";
			var result3 = userManager.Create(user3, password3);

			if (result3.Succeeded)
			{
				userManager.AddToRole(user3.Id, role2.Name);
			}

			base.Seed(context);
		}

		private List<Category> GetDefaultCategories()
		{
			return new List<Category>()
			{
				new Category { Name = "Numismatics" },
				new Category { Name = "Notaphily" },
				new Category { Name = "Philately" },
				new Category { Name = "Phaleristics" },
				new Category { Name = "Scale models" },
				new Category { Name = "Books" },
				new Category { Name = "Other" },
			};
		}

		private List<Subcategory> GetDefaultSubcategories()
		{
			return new List<Subcategory>()
			{
				new Subcategory { Name = "Gold, platinum and palladium coins after 1700",  CategoryId = 1 },
				new Subcategory { Name = "US coins",  CategoryId = 1 },
				new Subcategory { Name = "UK coins",  CategoryId = 1 },
				new Subcategory { Name = "Germany",  CategoryId = 1 },
				new Subcategory { Name = "France",  CategoryId = 1 },
				new Subcategory { Name = "Russia",  CategoryId = 1 },
				new Subcategory { Name = "other Europe",  CategoryId = 1 },
				new Subcategory { Name = "Asia",  CategoryId = 1 },
				new Subcategory { Name = "Africa",  CategoryId = 1 },
				new Subcategory { Name = "North America",  CategoryId = 1 },
				new Subcategory { Name = "South America",  CategoryId = 1 },
				new Subcategory { Name = "Oceania",  CategoryId = 1 },
				new Subcategory { Name = "Medieval coins",  CategoryId = 1 },
				new Subcategory { Name = "Ancient coins",  CategoryId = 1 },
				new Subcategory { Name = "Copies, fakes",  CategoryId = 1 },
				new Subcategory { Name = "Other",  CategoryId = 1 },

				new Subcategory { Name = "Europe",  CategoryId = 2 },
				new Subcategory { Name = "Asia",  CategoryId = 2 },
				new Subcategory { Name = "Africa",  CategoryId = 2 },
				new Subcategory { Name = "North America",  CategoryId = 2 },
				new Subcategory { Name = "South America",  CategoryId = 2 },
				new Subcategory { Name = "Oceania",  CategoryId = 2 },
				new Subcategory { Name = "Checks",  CategoryId = 2 },
				new Subcategory { Name = "Stocks, bonds, lotteries",  CategoryId = 2 },
				new Subcategory { Name = "Copies, fakes",  CategoryId = 2 },
				new Subcategory { Name = "Other",  CategoryId = 2 },

				new Subcategory { Name = "Europe",  CategoryId = 3 },
				new Subcategory { Name = "Asia",  CategoryId = 3 },
				new Subcategory { Name = "Africa",  CategoryId = 3 },
				new Subcategory { Name = "North America",  CategoryId = 3 },
				new Subcategory { Name = "South America",  CategoryId = 3 },
				new Subcategory { Name = "Oceania",  CategoryId = 3 },
				new Subcategory { Name = "Envelopes, postcards",  CategoryId = 3 },
				new Subcategory { Name = "Other",  CategoryId = 3 },

				new Subcategory { Name = "Awards, medals",  CategoryId = 4 },
				new Subcategory { Name = "Insignias",  CategoryId = 4 },
				new Subcategory { Name = "Tokens",  CategoryId = 4 },
				new Subcategory { Name = "Badges",  CategoryId = 4 },
				new Subcategory { Name = "Copies of medals and badges",  CategoryId = 4 },
				new Subcategory { Name = "Other",  CategoryId = 4 },

				new Subcategory { Name = "Aviation",  CategoryId = 5 },
				new Subcategory { Name = "Auto/Moto",  CategoryId = 5 },
				new Subcategory { Name = "Military equipment",  CategoryId = 5 },
				new Subcategory { Name = "Railroad",  CategoryId = 5 },
				new Subcategory { Name = "Figurines",  CategoryId = 5 },
				new Subcategory { Name = "Fleet",  CategoryId = 5 },
				new Subcategory { Name = "Other",  CategoryId = 5 },

				new Subcategory { Name = "Antique publications",  CategoryId = 6 },
				new Subcategory { Name = "Modern publications",  CategoryId = 6 },
				new Subcategory { Name = "Other",  CategoryId = 6 },

			};
		}

		private List<Country> GetDefaultCountries()
		{
			string filePath = Path.Combine(Directory.GetParent(AppDomain.CurrentDomain.BaseDirectory).Parent.FullName, "Data Access Layer", "Content", "Countries.txt");

			List<Country> countries = new List<Country>();

			using (StreamReader streamReader = new StreamReader(filePath, System.Text.Encoding.Default))
			{
				while (true)
				{
					string countryName = streamReader.ReadLine();

					if (countryName is null)
						break;

					countries.Add(new Country { Name = countryName });
				}
			}

			return countries;
		}
	}
}
