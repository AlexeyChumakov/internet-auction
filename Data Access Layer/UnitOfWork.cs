﻿using Data_Access_Layer.Interfaces;
using Data_Access_Layer.Repositories;
using System.Threading.Tasks;

namespace Data_Access_Layer
{
	public sealed class UnitOfWork : IUnitOfWork
	{
		private readonly AuctionDbContext context;

		public ApplicationUserManager UserManager { get; }
		public ApplicationRoleManager RoleManager { get; }

		public ICountryRepository CountryRepository { get; }
		public IGenderRepository GenderRepository { get; }

		public ILotRepository LotRepository { get; }
		public IAuctionRepository AuctionRepository { get; }
		public IBidRepository BidRepository { get; }
		public IDurationRepository DurationRepository { get; }
		public ICategoryRepository CategoryRepository { get; }
		public ISubcategoryRepository SubcategoryRepository { get; }


		public UnitOfWork(string connectionString)
		{
			context = new AuctionDbContext(connectionString);

			UserManager = new ApplicationUserManager(context);
			RoleManager = new ApplicationRoleManager(context);

			CountryRepository = new CountryRepository(context);
			GenderRepository = new GenderRepository(context);

			LotRepository = new LotRepository(context);
			AuctionRepository = new AuctionRepository(context);
			BidRepository = new BidRepository(context);
			DurationRepository = new DurationRepository(context);
			CategoryRepository = new CategoryRepository(context);
			SubcategoryRepository = new SubcategoryRepository(context);
		}

		public async Task<int> SaveAsync()
		{
			return await context.SaveChangesAsync();
		}

		public void Dispose()
		{
			UserManager?.Dispose();
			RoleManager?.Dispose();
		}
	}
}
