﻿using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
	class CategoryRepository : ICategoryRepository
	{
		private readonly AuctionDbContext context;

		public CategoryRepository(AuctionDbContext context)
		{
			this.context = context;
		}

		public async Task<IList<Category>> GetAllAsync()
		{
			return await context.Categories.ToListAsync();
		}

		public async Task<Category> GetByIdAsync(int id)
		{
			return await context.Categories.FindAsync(id);
		}

		public void Add(Category entity)
		{
			context.Categories.Add(entity);
		}

		public void Delete(Category entity)
		{
			context.Categories.Remove(entity);
		}

		public async Task DeleteByIdAsync(int id)
		{
			Category category = await GetByIdAsync(id);
			Delete(category);
		}

		public void Update(Category entity)
		{
			context.Entry(entity).State = EntityState.Modified;
		}
	}
}
