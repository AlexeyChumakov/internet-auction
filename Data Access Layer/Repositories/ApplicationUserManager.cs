﻿using Data_Access_Layer.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data_Access_Layer.Repositories
{
    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        public ApplicationUserManager(AuctionDbContext context) : base(new UserStore<ApplicationUser>(context))
        {
        }
    }
}
