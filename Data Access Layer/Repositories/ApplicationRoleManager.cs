﻿using Data_Access_Layer.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Data_Access_Layer.Repositories
{
    public class ApplicationRoleManager : RoleManager<ApplicationRole>
    {
        public ApplicationRoleManager(AuctionDbContext context) : base(new RoleStore<ApplicationRole>(context))
        { 
        }
    }
}
