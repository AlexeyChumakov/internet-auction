﻿using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
	class DurationRepository : IDurationRepository
	{
		private readonly AuctionDbContext context;

		public DurationRepository(AuctionDbContext context)
		{
			this.context = context;
		}

		public async Task<IList<Duration>> GetAllAsync()
		{
			return await context.Durations.ToListAsync();
		}

		public async Task<Duration> GetByIdAsync(int id)
		{
			return await context.Durations.FindAsync(id);
		}

		public void Add(Duration entity)
		{
			context.Durations.Add(entity);
		}

		public void Delete(Duration entity)
		{
			context.Durations.Remove(entity);
		}

		public async Task DeleteByIdAsync(int id)
		{
			Duration duration = await GetByIdAsync(id);
			Delete(duration);
		}

		public void Update(Duration entity)
		{
			context.Entry(entity).State = EntityState.Modified;
		}
	}
}
