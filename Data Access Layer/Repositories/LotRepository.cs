﻿using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
	class LotRepository : ILotRepository
	{
		private readonly AuctionDbContext context;

		public LotRepository(AuctionDbContext context)
		{
			this.context = context;
		}

		public async Task<IList<Lot>> GetAllAsync()
		{
			return await context.Lots.ToListAsync();
		}

		public async Task<Lot> GetByIdAsync(int id)
		{
			return await context.Lots.FindAsync(id);
		}

		public void Add(Lot entity)
		{
			context.Lots.Add(entity);
		}

		public void Delete(Lot entity)
		{
			context.Lots.Remove(entity);
		}

		public async Task DeleteByIdAsync(int id)
		{
			Lot lot = await GetByIdAsync(id);
			Delete(lot);
		}

		public void Update(Lot entity)
		{
			context.Entry(entity).State = EntityState.Modified;
		}
	}
}
