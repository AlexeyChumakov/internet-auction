﻿using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
	class CountryRepository : ICountryRepository
	{
		private readonly AuctionDbContext context;

		public CountryRepository(AuctionDbContext context)
		{
			this.context = context;
		}

		public async Task<IList<Country>> GetAllAsync()
		{
			return await context.Countries.ToListAsync();
		}

		public async Task<Country> GetByIdAsync(int id)
		{
			return await context.Countries.FindAsync(id);
		}

		public void Add(Country entity)
		{
			context.Countries.Add(entity);
		}

		public void Delete(Country entity)
		{
			context.Countries.Remove(entity);
		}

		public async Task DeleteByIdAsync(int id)
		{
			Country countries = await GetByIdAsync(id);
			Delete(countries);
		}

		public void Update(Country entity)
		{
			context.Entry(entity).State = EntityState.Modified;
		}
	}
}
