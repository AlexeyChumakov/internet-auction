﻿using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
	class GenderRepository : IGenderRepository
	{
		private readonly AuctionDbContext context;

		public GenderRepository(AuctionDbContext context)
		{
			this.context = context;
		}

		public async Task<IList<Gender>> GetAllAsync()
		{
			return await context.Genders.ToListAsync();
		}

		public async Task<Gender> GetByIdAsync(int id)
		{
			return await context.Genders.FindAsync(id);
		}

		public void Add(Gender entity)
		{
			context.Genders.Add(entity);
		}

		public void Delete(Gender entity)
		{
			context.Genders.Remove(entity);
		}

		public async Task DeleteByIdAsync(int id)
		{
			Gender gender = await GetByIdAsync(id);
			Delete(gender);
		}

		public void Update(Gender entity)
		{
			context.Entry(entity).State = EntityState.Modified;
		}
	}
}
