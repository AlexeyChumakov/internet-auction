﻿using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
	class AuctionRepository : IAuctionRepository
	{
		private readonly AuctionDbContext context;

		public AuctionRepository(AuctionDbContext context)
		{
			this.context = context;
		}

		public async Task<IList<Auction>> GetAllAsync()
		{
			return await context.Auctions.ToListAsync();
		}

		public async Task<Auction> GetByIdAsync(int id)
		{
			return await context.Auctions.FindAsync(id);
		}

		public void Add(Auction entity)
		{
			context.Auctions.Add(entity);
		}

		public void Delete(Auction entity)
		{
			context.Auctions.Remove(entity);
		}

		public async Task DeleteByIdAsync(int id)
		{
			Auction auction = await GetByIdAsync(id);
			Delete(auction);
		}

		public void Update(Auction entity)
		{
			context.Entry(entity).State = EntityState.Modified;
		}
	}
}
