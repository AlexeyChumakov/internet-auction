﻿using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
	class BidRepository : IBidRepository
	{
		private readonly AuctionDbContext context;

		public BidRepository(AuctionDbContext context)
		{
			this.context = context;
		}

		public async Task<IList<Bid>> GetAllAsync()
		{
			return await context.Bids.ToListAsync();
		}

		public async Task<Bid> GetByIdAsync(int id)
		{
			return await context.Bids.FindAsync(id);
		}

		public void Add(Bid entity)
		{
			context.Bids.Add(entity);
		}

		public void Delete(Bid entity)
		{
			context.Bids.Remove(entity);
		}

		public async Task DeleteByIdAsync(int id)
		{
			Bid bid = await GetByIdAsync(id);
			Delete(bid);
		}

		public void Update(Bid entity)
		{
			context.Entry(entity).State = EntityState.Modified;
		}
	}
}
