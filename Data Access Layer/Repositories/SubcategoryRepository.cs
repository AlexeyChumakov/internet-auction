﻿using Data_Access_Layer.Entities;
using Data_Access_Layer.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Data_Access_Layer.Repositories
{
	class SubcategoryRepository : ISubcategoryRepository
	{
		private readonly AuctionDbContext context;

		public SubcategoryRepository(AuctionDbContext context)
		{
			this.context = context;
		}

		public async Task<IList<Subcategory>> GetAllAsync()
		{
			return await context.Subcategories.ToListAsync();
		}

		public async Task<Subcategory> GetByIdAsync(int id)
		{
			return await context.Subcategories.FindAsync(id);
		}

		public void Add(Subcategory entity)
		{
			context.Subcategories.Add(entity);
		}

		public void Delete(Subcategory entity)
		{
			context.Subcategories.Remove(entity);
		}

		public async Task DeleteByIdAsync(int id)
		{
			Subcategory subcategory = await GetByIdAsync(id);
			Delete(subcategory);
		}

		public void Update(Subcategory entity)
		{
			context.Entry(entity).State = EntityState.Modified;
		}
	}
}
