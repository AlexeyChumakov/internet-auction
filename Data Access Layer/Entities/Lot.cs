﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data_Access_Layer.Entities
{
	public class Lot : BaseEntity
	{
		[Required]
		[StringLength(10, MinimumLength = 100)]
		public string Name { get; set; }


		public string Сondition { get; set; }
		public bool Restored { get; set; }
		public string Defects { get; set; }

		public string PaymentType { get; set; }
		public string Location { get; set; }
		public string ShippingDeliveryOptions { get; set; }
		public bool SendingLotAbroad { get; set; }

		[StringLength(10, MinimumLength = 9000)]
		public string Description { get; set; }


		public virtual Auction Auction { get; set; }

		[Required]
		[ForeignKey("ApplicationUser")]
		public string SellerId { get; set; }

		public virtual ApplicationUser ApplicationUser { get; set; }

		[Required]
		[ForeignKey("Category")]
		public int CategoryId { get; set; }
		public virtual Category Category { get; set; }

		[ForeignKey("Subcategory")]
		public int SubcategoryId { get; set; }
		public virtual Subcategory Subcategory { get; set; }
	}
}
