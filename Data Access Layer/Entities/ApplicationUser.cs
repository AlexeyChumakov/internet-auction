﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data_Access_Layer.Entities
{
	public class ApplicationUser : IdentityUser
	{
		[Required]
		[StringLength(35, MinimumLength = 1)]
		public string Name { get; set; }

		[Required]
		[StringLength(35, MinimumLength = 1)]
		public string Surname { get; set; }

		[Required]
		[StringLength(16, MinimumLength = 8)]
		public override string PhoneNumber { get => base.PhoneNumber; set => base.PhoneNumber = value; }

		[Required]
		[Column(TypeName = "date")]
		public DateTime Birthdate { get; set; }
		
		public string AboutMe { get; set; }

		[Required]
		[ForeignKey("Country")]
		public int CountryId { get; set; }

		[Required]
		[ForeignKey("Gender")]
		public int GenderId { get; set; }


		public virtual Country Country { get; set; }

		public virtual Gender Gender { get; set; }

		public virtual ICollection<Lot> Lots { get; set; }
	}
}
