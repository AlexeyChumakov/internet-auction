﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data_Access_Layer.Entities
{
	public class Gender : BaseEntity
	{
		[Required]
		[StringLength(50, MinimumLength = 3)]
		[Index(IsUnique = true)]
		public string Name { get; set; }

		public virtual ICollection<ApplicationUser> Users { get; set; }
	}
}
