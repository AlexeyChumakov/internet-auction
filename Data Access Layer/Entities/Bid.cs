﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data_Access_Layer.Entities
{
	public class Bid : BaseEntity
	{
		public decimal PriceIncrement { get; set; }

		public DateTime DateTime { get; set; }

		public bool IsWinner { get; set; }

		[Required]
		[ForeignKey("ApplicationUser")]
		public string BidderId { get; set; }
		public virtual ApplicationUser ApplicationUser { get; set; }

		[Required]
		[ForeignKey("Auction")]
		public int AuctionId { get; set; }
		public virtual Auction Auction { get; set; }
	}
}