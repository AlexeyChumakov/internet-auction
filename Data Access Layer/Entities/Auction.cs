﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data_Access_Layer.Entities
{
	public class Auction
	{
		[Key]
		[ForeignKey("Lot")]
		public int LotId { get; set; }

		public DateTime StartDate { get; set; }
		public DateTime EndDate { get; set; }

		public decimal StartPrice { get; set; }


		[Required]
		[ForeignKey("Duration")]
		public int DurationId { get; set; }
		public virtual Duration Duration { get; set; }

		public virtual Lot Lot { get; set; }

		public virtual ICollection<Bid> Bids { get; set; }
	}
}
