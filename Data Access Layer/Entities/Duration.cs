﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data_Access_Layer.Entities
{
	public class Duration : BaseEntity
	{
		[Required]
		public int Days { get; set; }

		public virtual ICollection<Auction> Auctions { get; set; }
	}
}