﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data_Access_Layer.Entities
{
	public class Subcategory : BaseEntity
	{
		[Required]
		public string Name { get; set; }

		[Required]
		[ForeignKey("Category")]
		public int CategoryId { get; set; }

		public virtual Category Category { get; set; }

		public virtual ICollection<Lot> Lots { get; set; }
	}
}