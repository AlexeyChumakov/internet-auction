﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Data_Access_Layer.Entities
{
	public class Category : BaseEntity
	{
		[Required]
		public string Name { get; set; }

		public virtual ICollection<Subcategory> Subcategories { get; set; }

		public virtual ICollection<Lot> Lots { get; set; }
	}
}
