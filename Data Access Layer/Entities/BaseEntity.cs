﻿using System.ComponentModel.DataAnnotations;

namespace Data_Access_Layer.Entities
{
	public abstract class BaseEntity
	{
		[Key]
		public int Id { get; set; }
	}
}
