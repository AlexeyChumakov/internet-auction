﻿using Data_Access_Layer.Entities;

namespace Data_Access_Layer.Interfaces
{
	public interface IAuctionRepository : IRepository<Auction>
	{
	}
}
