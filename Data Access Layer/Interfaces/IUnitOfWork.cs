﻿using Data_Access_Layer.Repositories;
using System;
using System.Threading.Tasks;

namespace Data_Access_Layer.Interfaces
{
	public interface IUnitOfWork : IDisposable
	{
		ApplicationUserManager UserManager { get; }
		ApplicationRoleManager RoleManager { get; }

		IGenderRepository GenderRepository { get; }
		ICountryRepository CountryRepository { get; }

		ILotRepository LotRepository { get; }
		IAuctionRepository AuctionRepository { get; }
		IBidRepository BidRepository { get; }
		IDurationRepository DurationRepository { get; }
		ICategoryRepository CategoryRepository { get; }
		ISubcategoryRepository SubcategoryRepository { get; }

		Task<int> SaveAsync();
	}
}
