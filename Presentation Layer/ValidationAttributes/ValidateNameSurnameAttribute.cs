﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Presentation_Layer.ValidationAttributes
{
	public class ValidateNameSurnameAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            string nameOrSurname = value as string;

            if (char.IsUpper(nameOrSurname.First()) && nameOrSurname.All(x => char.IsLetter(x)) && nameOrSurname.Skip(1).All(x => char.IsLower(x)))
                return true;
            else
                return false;
        }
    }
}