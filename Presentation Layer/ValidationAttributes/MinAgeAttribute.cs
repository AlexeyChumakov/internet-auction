﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Presentation_Layer.ValidationAttributes
{
    public class MinAgeAttribute : ValidationAttribute
    {
        public int AgeInYears { get; set; }

        public MinAgeAttribute(int ageInYears) => AgeInYears = ageInYears;

        public override bool IsValid(object value)
        {
            DateTime birthdate = (DateTime)value;

            if (DateTime.Today >= birthdate.AddYears(AgeInYears))
                return true;

            return false;
        }
    }
}