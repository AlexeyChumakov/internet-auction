﻿using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Presentation_Layer.ValidationAttributes
{
	public class ValidatePasswordAttribute : ValidationAttribute
	{
        public override bool IsValid(object value)
        {
            string password = value as string;

            if (string.IsNullOrEmpty(password))
                return false;
            else if (password.Any(x => char.IsUpper(x)) && password.Any(x => char.IsLower(x)) && password.Any(x => char.IsDigit(x)))
                return true;
            else
                return false;
        }
    }
}