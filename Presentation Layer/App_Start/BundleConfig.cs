﻿using System.Web.Optimization;

namespace Presentation_Layer
{
	public static class BundleConfig
	{
		public static void RegisterBundles(BundleCollection bundles)
		{
			bundles.Add(new StyleBundle("~/css").Include("~/Content/Site.css"));
			bundles.Add(new ScriptBundle("~/jquery").Include("~/Scripts/jquery-{version}.js"));
			bundles.Add(new ScriptBundle("~/jquery/validate").Include("~/Scripts/jquery.validate*"));
			bundles.Add(new ScriptBundle("~/bootstrap/scripts").Include("~/Scripts/bootstrap.js"));
			bundles.Add(new StyleBundle("~/bootstrap/styles").Include("~/Content/bootstrap.css"));
		}
	}
}
