using Business_Logic_Layer.IoC_Modules;
using Ninject;
using Ninject.Modules;
using Ninject.Web.Mvc;
using System;
using System.Configuration;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Presentation_Layer
{
	public class MvcApplication : System.Web.HttpApplication
	{
		protected void Application_Start()
		{
			AreaRegistration.RegisterAllAreas();
			FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
			RouteConfig.RegisterRoutes(RouteTable.Routes);
			BundleConfig.RegisterBundles(BundleTable.Bundles);

			NinjectModule homeModule = new HomeModule();
			NinjectModule userModule = new UserModule();
			NinjectModule serviceModule = new ServiceModule(ConfigurationManager.ConnectionStrings["DatabaseConfig"].ConnectionString);
			NinjectModule automapperModule = new AutomapperModule();
			var kernel = new StandardKernel(homeModule, userModule, serviceModule, automapperModule);
			DependencyResolver.SetResolver(new NinjectDependencyResolver(kernel));
		}
	}
}
