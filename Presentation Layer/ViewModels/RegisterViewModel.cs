﻿using Presentation_Layer.ValidationAttributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using CompareAttribute = System.ComponentModel.DataAnnotations.CompareAttribute;

namespace Presentation_Layer.ViewModels
{
	public class RegisterViewModel
	{
        [Required(ErrorMessage = "This field is required")]
        [StringLength(50, MinimumLength = 3, ErrorMessage = "Username must be at least 3 characters long or be max of 50 characters long")]
        [RegularExpression(@"^(?!.*[-_]{2,})(?=^[^-_].*[^-_]$)[\w-]{3,50}$", ErrorMessage = @"The username can't contain whitespaces and special characters except for underscore and hyphen (""_"" and ""-"")")]
        public string Username { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [StringLength(35, MinimumLength = 1, ErrorMessage = "Name must be max of 35 characters long")]
        [ValidateNameSurname(ErrorMessage = "The name can only consist of letters and start with an uppercase letter else letters are lowercase")]
        public string Name { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [StringLength(35, MinimumLength = 1, ErrorMessage = "Surname must be max of 35 characters long")]
        [ValidateNameSurname(ErrorMessage = "The surname can only consist of letters and start with an uppercase letter else letters are lowercase")]
        public string Surname { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [StringLength(16, MinimumLength = 8, ErrorMessage = "The phone number can contain only 7-15 digits")]
        [RegularExpression(@"^\++?[1-9][0-9]\d{5,13}$", ErrorMessage = "The phone number must start with '+' and a nonzero digit and containt only digits")]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [EmailAddress]
        public string Email { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 6, ErrorMessage = "Password must be at least 6 characters long or be max of 50 characters long")]
        [ValidatePassword(ErrorMessage = "Password must contain an uppercase letter, a lowercase letter, and a digit")]
        public string Password { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }

        [StringLength(2000, ErrorMessage = "About me field must be max of 50 characters long")]
        public string AboutMe { get; set; }

        [Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Date)]
        [MinAge(18, ErrorMessage = "You are not yet 18 years old")]
        public DateTime Birthdate { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public int GenderId { get; set; }

        [Required(ErrorMessage = "This field is required")]
        public int CountryId { get; set; }



        public SelectList Genders { get; set; }
        public SelectList Countries { get; set; }
    }
}