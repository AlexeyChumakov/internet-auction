﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Presentation_Layer.ViewModels
{
    public class LoginViewModel
    {
        [Required(ErrorMessage = "This field is required")]
        [Remote("ValidateLoginOrEmail", "Account")]
        public string LoginOrEmail { get; set; }

		[Required(ErrorMessage = "This field is required")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}