﻿using Business_Logic_Layer.Models;
using System.Collections.Generic;

namespace Presentation_Layer.ViewModels
{
	public class HomeViewModel
	{
		public IList<CategoryModel> Categories { get; set; }
	}
}