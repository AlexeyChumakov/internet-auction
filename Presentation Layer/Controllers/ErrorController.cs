﻿using System.Web.Mvc;

namespace Presentation_layer.Controllers
{
    public class ErrorController : Controller //Controller for outputting error statuses for users
    {
        public ActionResult NotFound()
        {
            Response.StatusCode = 404;
            return View();
        }

        public ActionResult InternalServerError()
        {
            Response.StatusCode = 500;
            return View();
        }
    }
}