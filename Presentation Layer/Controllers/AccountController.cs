﻿using AutoMapper;
using Business_Logic_Layer.Infrastructure;
using Business_Logic_Layer.Interfaces;
using Business_Logic_Layer.Models;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Presentation_Layer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Presentation_Layer.Controllers
{
    public class AccountController : Controller
    {
		private IUserService service => HttpContext.GetOwinContext().GetUserManager<IUserService>();
		private IAuthenticationManager AuthenticationManager => HttpContext.GetOwinContext().Authentication;
        private IMapper mapper => new Mapper(new MapperConfiguration(cfg =>
        {
            cfg.CreateMap<UserModel, RegisterViewModel>().ReverseMap();
        }));

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                UserModel userDto;

               if (viewModel.LoginOrEmail.Contains("@"))
				{
                    userDto = new UserModel { Email = viewModel.LoginOrEmail, Password = viewModel.Password };
                }
               else
				{
                    userDto = new UserModel { UserName = viewModel.LoginOrEmail, Password = viewModel.Password };
                }

                
                ClaimsIdentity claim = await service.Authenticate(userDto);
                if (claim is null)
                {
                    ModelState.AddModelError("", "Неверный логин или пароль.");
                }
                else
                {
                    AuthenticationManager.SignOut();
                    AuthenticationManager.SignIn(new AuthenticationProperties
                    {
                        IsPersistent = true
                    }, claim);
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(viewModel);
        }

        public ActionResult Logout()
        {
            AuthenticationManager.SignOut();
            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Register()
        {
            return View(new RegisterViewModel { Countries = new SelectList(await service.GetAllCountriesAsync(), "Id", "Name"), Genders = new SelectList(await service.GetAllGendersAsync(), "Id", "Name") });
        }

		[HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel viewModel)
        {
            if (ModelState.IsValid)
            {
                UserModel userModel = mapper.Map<RegisterViewModel, UserModel>(viewModel);

                userModel.Role = "User";

                List<RegistrationError> registrationErrors = (await service.Create(userModel)).ToList();

                if (!registrationErrors.Any())
                    return View("SuccessRegister");
                else
                    foreach(RegistrationError registrationError in registrationErrors)
                        ModelState.AddModelError(registrationError.Property, registrationError.Message);
            }

            
            viewModel.Countries = new SelectList(await service.GetAllCountriesAsync(), "Id", "Name");
            viewModel.Genders = new SelectList(await service.GetAllGendersAsync(), "Id", "Name");

            return View(viewModel);
        }

        public JsonResult ValidateLoginOrEmail(string loginOrEmail)
        {
            if(loginOrEmail.Contains("@"))
			{
                if (Regex.IsMatch(loginOrEmail,
                    @"^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$",
                     RegexOptions.Compiled | RegexOptions.IgnoreCase | RegexOptions.ExplicitCapture))
                {
                    return Json(true, JsonRequestBehavior.AllowGet);
                }

                return Json("Write an email in the correct form", JsonRequestBehavior.AllowGet);
            }
            else
			{
                if(loginOrEmail.Length < 3)
                    return Json(@"Username must be at least 3 characters long", JsonRequestBehavior.AllowGet);
                else if (loginOrEmail.Length > 50)
                    return Json(@"Username can be max of 50 characters long", JsonRequestBehavior.AllowGet);
                else if (Regex.IsMatch(loginOrEmail, @"^(?!.*[-_]{2,})(?=^[^-_].*[^-_]$)[\w-]{3,50}$"))
                    return Json(true, JsonRequestBehavior.AllowGet);

                return Json(@"The username can't contain whitespaces and special characters except for underscore and hyphen (""_"" and ""-"")", JsonRequestBehavior.AllowGet);
            }
        }
    }
}