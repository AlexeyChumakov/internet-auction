﻿using Business_Logic_Layer.Interfaces;
using Presentation_Layer.ViewModels;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Presentation_Layer.Controllers
{
	public class HomeController : Controller
	{
		private readonly IHomeService service;

		public HomeController(IHomeService service) => this.service = service;

		public async Task<ActionResult> Index()
		{
			return View(new HomeViewModel { Categories = await service.GetAllCategoriesAsync() });
		}
	}
}